				<!-- BEGIN footer -->
				<footer>
					<!-- BEGIN Tertiary content -->
					<?php get_sidebar('footer'); ?>
					<!-- END Tertiary content -->

					<!-- BEGIN Footer navigation -->
					<?php createNavigation('footer-navigation', 'footer-navigation'); ?>
					<!-- END Footer navigation -->
				</footer>
				<!-- END footer -->

				<!-- BEGIN Footer scripts -->
				<!-- BEGIN jQuery script -->
				<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
				<script type="text/javascript">window.jQuery || document.write('<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/libs/jquery-1.10.1.min.js"><\/script>')</script>
				<!-- END jQuery script -->
				<!-- BEGIN Plugin and custom scripts -->
				<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/plugins/slides.min.jquery.js"></script>
				<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/script.js"></script>
				<!-- END Plugin and custom scripts -->
				<!-- BEGIN Google Analytics script -->
				<script type="text/javascript">
					var _gaq = _gaq || [];
						_gaq.push(['_setAccount', 'UA-XXXXX-X']);
						_gaq.push(['_trackPageview']);

					(function() {
						var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
						ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
						var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
					})();
				</script>
				<!-- END Google Analytics script -->
				<!-- BEGIN Google+ script -->
				<script type="text/javascript">
					(function() {
						var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
						po.src = 'https://apis.google.com/js/plusone.js';
						var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
					})();
				</script>
				<!-- END Google+ script -->
				<!-- END Footer scripts -->

<?php wp_footer(); ?>
			</div>
			<!-- END grids -->
		</div>
		<!-- END container -->
	</body>
	<!-- END body -->
</html>
<!-- END html -->