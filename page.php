<?php get_header() ?>

				<!-- BEGIN Page content -->
				<div role="main">
					<?php if (have_posts()) while (have_posts()) : the_post(); ?>
					<article>
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
						<?php edit_post_link(); ?>
					</article>
					<?php endwhile; ?>
				</div>
				<!-- END Page content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>