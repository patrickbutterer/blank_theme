<?php get_header() ?>

				<!-- BEGIN Page content -->
				<div role="main">
					<h1>Nicht gefunden!</h1>
					<p>Leider konnte die von dir eingegebene Seite nicht gefunden werden.</p>
				</div>
				<!-- END Page content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>