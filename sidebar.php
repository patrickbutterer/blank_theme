				<!-- BEGIN Secondary content -->
				<aside role="complementary">
					<?php if (is_active_sidebar('sidebar-widget-area')) : ?>
					<ul>
						<?php dynamic_sidebar('sidebar-widget-area'); ?>
					</ul>
					<?php endif; ?>
				</aside>
				<!-- END Secondary content -->