<?php
	add_theme_support( 'post-thumbnails' );

	add_filter('nav_menu_css_class', 'changeCssAttributes', 100, 1);
	add_filter('nav_menu_item_id', 'changeCssAttributes', 100, 1);
	add_filter('page_css_class', 'changeCssAttributes', 100, 1);

	function changeCssAttributes($var) {
  		if(is_array($var)){
                $var1 = array_intersect($var, array('current-menu-item'));
                $var2 = array('current-menu-item');
                $var3   = array('active');
                $var = array();
                $var = str_replace($var2, $var3, $var1);
            }
            else{
                $var= '';
            }
        return $var;
	}

	function createNavigation($theme_location, $container_id) {
    	$options = array(
    		'theme_location' => $theme_location,
			'container' => 'nav',
			'container_class' => ' ',
			'container_id' => $container_id,
			'echo' => false,
			'items_wrap' => '<ul>%3$s</ul>'
    	);
    	$nav = wp_nav_menu($options);
    	$nav = str_replace('class=" "', '', $nav);
    	echo $nav;
	}

	register_nav_menus(array(
		'main-navigation' => 'Hauptnavigation',
		'footer-navigation' => 'Footer-Navigation'
	));

	register_sidebar(array(
		'name' => __('Sidebar Widget-Bereich'),
		'id' => 'sidebar-widget-area',
		'before_widget' => '<li>',
		'after_widget' => '</li>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));

	register_sidebar(array(
		'name' => __('Footer Widget-Bereich'),
		'id' => 'footer-widget-area',
		'before_widget' => '<li>',
		'after_widget' => '</li>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
?>