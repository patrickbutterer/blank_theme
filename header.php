<!DOCTYPE html>
<!-- BEGIN html -->
<!--[if lt IE 7]>		<html lang="de" class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>			<html lang="de" class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>			<html lang="de" class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>			<html lang="de" class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!-->	<html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->
	
	<!-- BEGIN head -->
	<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# APP_NAMESPACE: http://ogp.me/ns/fb/APP_NAMESPACE#">		
		<!-- Title -->
		<title><?php bloginfo('name'); ?></title>

		<!-- BEGIN Meta tags -->
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
		<meta name="description" content="" />
		<meta name="author" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<!-- END Meta tags -->

		<!-- BEGIN Icons -->
		<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.ico" />
		<link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/img/apple-touch-icon.png" />
		<!-- END Icons -->

		<!-- BEGIN Styles -->
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
		<!-- Google Web Fonts -->
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=FONT_NAME:FONT_WEIGHTS" />
		<!-- END Styles -->

		<!-- BEGIN Head scripts -->
		<!-- Modernizr -->
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/libs/modernizr-2.6.2.min.js"></script>
		<!-- END Head scripts -->

		<!-- BEGIN Open Graph meta properties -->
		<meta property="fb:app_id" content="APP_ID" />
		<meta property="fb:admins" content="ADMIN_ID" />
		<meta property="og:url" content="<?php the_permalink() ?>" />
		<meta property="og:site_name" content="<?php bloginfo('name'); ?>" />
<?php if (is_single() || is_page()) { ?>
		<meta property="og:type" content="article" />
		<meta property="og:image" content="<?php echo wp_get_attachment_thumb_url( get_post_thumbnail_id($post->ID)); ?>" />
		<meta property="og:title" content="<?php the_title(); ?>" />
		<meta property="og:description" content="<?php echo strip_tags(get_the_excerpt($post->ID)); ?>" />
<?php } else { ?>
		<meta property="og:type" content="website" />
		<meta property="og:image" content="<?php bloginfo('template_directory'); ?>/img/facebook.png" />
		<meta property="og:description" content="<?php bloginfo('description'); ?>" />
<?php } ?>
		<!-- END Open Graph meta properties -->

<?php wp_head(); ?>
	</head>
	<!-- END head -->

	<!-- BEGIN body -->
	<body <?php body_class(); ?>>
		<!--[if lt IE 7]>
		<p class="chromeframe">Dein Browser ist veraltet. <a href="http://browsehappy.com/">Aktualisiere jetzt deinen Browser</a> oder <a href="http://www.google.com/chromeframe/?hl=de&redirect=true">installiere Google Chrome Frame</a>, um diese Webseite aufzurufen.</p>
		<![endif]-->

		<!-- BEGIN Facebook script -->
		<div id="fb-root"></div>
		<script type="text/javascript">
			(function(d){
				var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement('script'); js.id = id; js.async = true;
				js.src = "//connect.facebook.net/de_DE/all.js#xfbml=1";
				ref.parentNode.insertBefore(js, ref);
			}(document));
		</script>
		<!-- END Facebook script -->

		<!-- BEGIN container -->
		<div id="container">
			<!-- BEGIN grids -->
			<div class="grids">
				<!-- BEGIN header -->
				<header>
					<div id="logo">
						<a href="<?php echo home_url('/'); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a>
					</div>

					<!-- BEGIN Main navigation -->
					<?php createNavigation('main-navigation', 'main-navigation'); ?>
					<!-- END Main navigation -->
				</header>
				<!-- END header -->