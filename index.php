<?php get_header() ?>

				<!-- BEGIN Page content -->
				<div role="main">
					<?php
						$query_featured = new WP_Query(array(
							'posts_per_page' => 1,
						));
					?>
					<?php if (have_posts()) : $query_featured->the_post(); ?>
					<article class="featured">
						<?php
							if (has_post_thumbnail()) {
								the_post_thumbnail();
							}
						?>
						<h1>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
						</h1>
						xxx
						<?php /*the_content();*/ ?>
						<?php edit_post_link(); ?>
					</article>
					<hr />
					<?php endif; ?>
					<?php
						$query_posts = new WP_Query(array(
							'posts_per_page' => 5,
							'offset' => 1
						));
					?>
					<?php if (have_posts()) while ($query_posts->have_posts()) : $query_posts->the_post(); ?>
					<article>
						<?php
							if (has_post_thumbnail()) {
								the_post_thumbnail();
							}
						?>
						<header>
							<h1>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
							</h1>
						</header>
						xxx
						<?php /*the_content();*/ ?>
						<footer>
							<?php edit_post_link(); ?>
						</footer>
					</article>
					<hr />
					<?php endwhile; ?>
				</div>
				<!-- END Page content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>